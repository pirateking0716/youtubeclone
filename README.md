# React YouTube App CI/CD with GitLab

Automate your React YouTube app deployment using GitLab CI/CD pipelines. This guide takes you through each step, from API key creation to Docker container deployment on an EC2 instance.

## Steps

### Step 1: Create Rapid API Key

1. Sign up on [RapidAPI](https://rapidapi.com/).
2. Search for "YouTube v3" and obtain the API key.
3. Save the API key for Docker build.

### Step 2: Create GitLab Repository

1. Create a new GitLab project.
2. Push your React YouTube app code to the repository.

### Step 3: Launch EC2 Instance with SonarQube

1. Launch an EC2 instance on AWS.
2. Install Docker on the instance.
3. Run SonarQube container on port 9000.

### Step 4A: Create `.gitlab-ci.yml` File

1. Add a `.gitlab-ci.yml` file to your project.
2. Configure npm and SonarQube stages.

### Step 4B: Add Variables for SonarQube

1. Add SonarQube related variables in GitLab project settings.

### Step 5: Install GitLab Runner on EC2

1. Install GitLab Runner on your EC2 instance.
2. Register the runner with your GitLab project.

### Step 6: Run Application on Docker Container

1. Update `.gitlab-ci.yml` for Docker build and push stages.
2. Add Docker credentials to GitLab variables.
3. Run the CI/CD pipeline.

### Step 7: Access Application on Browser

1. Copy the public IP of your EC2 instance.
2. Open the application in your browser on port 3000.

### Step 8: Termination

1. Delete Docker Hub personal access token.
2. Stop and remove Docker containers.
3. Terminate the EC2 instance.

## Contributing

Feel free to contribute to enhance this CI/CD setup.